import React from 'react';
import {Link} from 'react-router-dom';

function About(props){
    return (
        <section>
        <div style={{border: "1px solid grey", padding:"5px"}}>
            <h1 align="center">Data Peserta Sanbercode Bootcamp Reactjs</h1>
            <ol>
                <li><b>Nama: </b>Diska Amalia Widyaningrum</li>
                <li><b>Email: </b>diskaamalia12@gmail.com</li>
                <li><b>Sistem Operasi yang digunakan: </b>Windows 10 </li>
                <li><b>Akun Gitlab: </b>https://gitlab.com/diskaamalia12</li>
                <li><b>Akun Telegram: </b>diska amalia</li>
            </ol>
        </div>
        <Link to="/">Kembali Ke Index</Link>
        </section>
    )
}

export default About;
