import React, { useContext } from 'react';
import {Link, BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Header from './Header';
import Routes from './Routes';
import './App.css';
import './tugas2.css';
import { MovieProvider } from './MovieContext';

const App = ()=>{
  return (
    <>
    <MovieProvider>
      <Router>
        <Header></Header>
        <Routes></Routes>
        <footer>
            <h5>copyright &copy; 2020 by Sanbercode</h5>
        </footer>
      </Router>
    </MovieProvider>
    </>
  );
}

export default App;
