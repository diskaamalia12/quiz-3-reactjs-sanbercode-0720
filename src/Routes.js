import React, { useContext } from "react";
import { Switch, Route } from "react-router";
import tugas2 from './tugas2';
import Login from './Login';
import Home from './Home';
import About from './About';
import ListMovieEditor from './ListMovie/Editor';
import { MovieContext } from './MovieContext';

const Routes = () => {
    const [currentLogin,setLogin] = useContext(MovieContext);

    let movieListRoute = "";

    if( currentLogin !== null ){
        movieListRoute = (
            <Route path="/movie-list-editor">
                <section><ListMovieEditor></ListMovieEditor></section>
            </Route>
        );
    }

    return (
        <Switch>
            <Route exact path="/">
                <Home></Home>
            </Route>
            <Route path="/tugas2">
                <tugas2></tugas2>
            </Route>
            <Route path="/about">
                <About></About>
            </Route>
            {movieListRoute}
            <Route path="/login">
                <Login></Login>
            </Route>
        </Switch>
    );

};

export default Routes;
